"use strict";

var _ethron = require("ethron");

var _child_process = _interopRequireDefault(require("child_process"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//imports
//Plugin task.
module.exports = exports = (0, _ethron.simple)({
  id: "eslint",
  desc: "Lint JavaScript code.",

  fmt(params) {
    return "ESLint: lint " + (params.src instanceof Array ? params.src : [params.src]).join(", ");
  },

  fmtParams(params) {
    return {
      src: params.length == 0 ? ["."] : params
    };
  }

}, function eslint(params, done) {
  var cmd = "./node_modules/.bin/eslint";
  if (params.conf) cmd += " -c " + params.conf;
  cmd += " " + (params.src instanceof Array ? params.src : [params.src]).join(" ");

  _child_process.default.exec(cmd, done);
});