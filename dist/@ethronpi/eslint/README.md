# @ethronpi/eslint

[![NPM version](https://img.shields.io/npm/v/@ethronpi/eslint.svg)](https://npmjs.org/package/@ethronpi/eslint)
[![Total downloads](https://img.shields.io/npm/dt/@ethronpi/eslint.svg)](https://npmjs.org/package/@ethronpi/eslint)

[Ethron.js](http://ethronlabs.com) plugin for [ESLint](https://eslint.org/).

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by EthronLabs.*

## Use

```
const eslint = require("@ethronpi/eslint");
```

## eslint task

This task runs `./node_modules/.bin/eslint`:

```
eslint({conf, src})
eslint(src)
```

- `conf` (string). `.eslintrc` to use. Default: `./.eslintrc`.
- `src` (string or string[], required). Source file(s) to parse.

Example:

```
eslint({
  src: ["Ethron.cat.js", "src", "test"]
});
```
