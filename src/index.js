//imports
import {simple} from "ethron";
import child_process from "child_process";

//Plugin task.
module.exports = exports = simple(
  {
    id: "eslint",
    desc: "Lint JavaScript code.",
    fmt(params) {
      return "ESLint: lint " + (params.src instanceof Array ? params.src : [params.src]).join(", ");
    },
    fmtParams(params) {
      return {src: (params.length == 0 ? ["."] : params)};
    }
  },

  function eslint(params, done) {
    var cmd = "./node_modules/.bin/eslint";
    if (params.conf) cmd += " -c " + params.conf;
    cmd += " " + (params.src instanceof Array ? params.src : [params.src]).join(" ");
    child_process.exec(cmd, done);
  }
);
