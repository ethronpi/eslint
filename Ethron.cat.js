//imports
const {cat} = require("ethron");
const babel = require("@ethronpi/babel");
const exec = require("@ethronpi/exec");
const fs = require("@ethronjs/plugin.fs");

//Package name.
const pkg = require("./package").name;

//catalog
cat.macro("lint", [
  [exec, "node_modules/.bin/eslint ."]
]).title("Lint source code");

cat.macro("trans-js", [
  [fs.rm, "dist"],
  [babel, "src", `dist/${pkg}/src/`]
]).title("Transpile from JS to JS");

cat.macro("build", [
  cat.get("lint"),
  cat.get("trans-js"),
  [fs.cp, "package.json", `dist/${pkg}/package.json`],
  [fs.cp, "README.md", `dist/${pkg}/README.md`]
]).title("Build package");

cat.macro("make", [
  cat.get("build")
]).title("Build");

cat.call("pub", exec, {
  cmd: `npm publish --access public dist/${pkg}`
}).title("Publish on NPM");

cat.macro("dflt", [
  cat.get("build")
]).title("Build");
